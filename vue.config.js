module.exports = {
  css: {
    loaderOptions: {
      scss: {
        prependData: ` 
          @import "node_modules/foundation-sites/scss/foundation.scss";
          @import "@/sass/_custom.scss";
          `,
      },
    },
  },
};
