
# Vue Movie Finder

  

### Simple Movie Finder In vue Search And Enjoy

  
#### Web Technologies I Used
![Vue js](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/18ee2fa0ef68e42cab7611df750f99d040ee62f9/img/Vue.svg)![Zurb Foundation](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/af994b0067174849a97046d4a3cd4f9632d7e0ce/img/zurb%20foundation.svg)![Netlify](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/3b7e2c6db845f61b47fd4bcc334c53c2094ee6ef/img/netlify.svg)

  

### Feature

  

1. Written In vue and Foundation

2. used Vue-router

3. Api Used [omdapi](omdbapi.com/)

  

### Future Feature

  

1. Adding Vuex

2. Better UI/UX

3. Adding Loading Bar

  

Click To see :- [Website](https://vue-movie-finder.netlify.app)

  

Reference link :- [https://www.youtube.com/watch?v=UHewcsv6uJY](https://www.youtube.com/watch?v=UHewcsv6uJY)

  

## Project setup

  

```
npm install
```

  

### Compiles and hot-reloads for development

  

```
npm run serve
```

  

### Compiles and minifies for production

  

```
npm run build
```

  

### Lints and fixes files

  

```
npm run lint
```
### For Netlify Hosting
#### initializing
```
# auto initializing
netlify init
# manual initializing
netlify init --manual
```
#### Deploy
```
#Draft
netlify deploy
#Porduction
netlify deploy --prod
```
  
### Customize configuration

  
See [Configuration Reference](https://cli.vuejs.org/config/).
See [Netlify Configuration Reference](https://docs.netlify.com/cli/get-started/)
