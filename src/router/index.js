import { createRouter, createWebHistory } from "vue-router";

// Importing Files
import Home from "../views/Home.vue";
import MovieDetail from "../views/MovieDetail.vue";
import NotFound from "../views/NotFound.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/movie/:id",
    name: "Movie Detail",
    component: MovieDetail,
  },
  {
    path: "/:Notfound(.*)",
    name: "Not Found",
    component: NotFound,
  },
];

// Createing Routing
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

// Exporting Routing
export default router;
