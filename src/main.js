import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

//Importing Document Title
import TitleMixin from "@/mixins/TitleMixin.js";

//Importing Foundation js File
import "../node_modules/foundation-sites/dist/js/foundation.js";

createApp(App).use(router).mixin(TitleMixin).mount("#app");
